﻿using Grpc.Net.Client;
using GrpcService1;
using System;
using System.Threading.Tasks;

namespace GrpcClient1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Test.TestClient(channel);
            var response = await client.TestMsgAsync(
                  new TestRequest
                  {
                      Name = "David He"
                  });
            Console.WriteLine("From Server: " + response.Message);
            Console.ReadKey();
        }

    }
}
